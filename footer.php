<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-6 -->
<a name="footer"></a>
<footer class="c-layout-footer c-layout-footer-6 c-bg-grey-1">

	<div class="container">

		<div class="c-prefooter c-bg-white">
			<div class="c-foot">
				<div class="row">
					<div class="col-md-7">
						<div class="c-content-title-1 c-title-md">
							<h3 class="c-font-uppercase c-font-bold">PT. TORABIKA EKA SEMESTA<br><br><br><span class="c-theme-font">ALAMAT :</span></h3>
							<div class="c-line-left hide"></div>
						</div>
						<p class="c-text c-font-16 c-font-regular">Jalan Raya Serang KM 12.5, Cikupa Tangerang, Banten, Indonesia.</p>
					</div>

				</div>
			</div>

		</div>

	</div>

	<div class="c-postfooter c-bg-dark-2">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 c-col">
					<p class="c-copyright c-font-grey">2019 &copy; PT. Torabika Eka Semesta
						<span class="c-font-grey-3">All Rights Reserved.</span>
					</p>
				</div>
			</div>
		</div>
	</div>

</footer>
<!-- END: LAYOUT/FOOTERS/FOOTER-6 -->

<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
	<i class="icon-arrow-up"></i>
</div><!-- END: LAYOUT/FOOTERS/GO2TOP -->

<!-- BEGIN: LAYOUT/BASE/BOTTOM -->
<!-- BEGIN: CORE PLUGINS -->
<!--[if lt IE 9]>
	<script src="public/assets/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
<script src="public/assets/plugins/jquery.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/jquery.easing.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/reveal-animate/wow.js" type="text/javascript"></script>
<script src="public/assets/demos/default/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript"></script>

<!-- END: CORE PLUGINS -->

<!-- BEGIN: LAYOUT PLUGINS -->
<script src="public/assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/revo-slider/js/extensions/revolution.extension.video.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="public/assets/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
<script src="public/assets/plugins/typed/typed.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
<script src="public/assets/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
<!-- END: LAYOUT PLUGINS -->

<!-- BEGIN: THEME SCRIPTS -->
<script src="public/assets/base/js/components.js" type="text/javascript"></script>
<script src="public/assets/base/js/components-shop.js" type="text/javascript"></script>
<script src="public/assets/base/js/app.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
		App.init(); // init core    
	});
</script>
<!-- END: THEME SCRIPTS -->

<!-- BEGIN: PAGE SCRIPTS -->
<script src="public/assets/demos/default/js/scripts/revo-slider/slider-4.js" type="text/javascript"></script>
<script src="public/assets/plugins/isotope/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/isotope/imagesloaded.pkgd.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/isotope/packery-mode.pkgd.min.js" type="text/javascript"></script>
<script src="public/assets/plugins/ilightbox/js/jquery.requestAnimationFrame.js" type="text/javascript"></script>
<script src="public/assets/plugins/ilightbox/js/jquery.mousewheel.js" type="text/javascript"></script>
<script src="public/assets/plugins/ilightbox/js/ilightbox.packed.js" type="text/javascript"></script>
<script src="public/assets/demos/default/js/scripts/pages/isotope-gallery.js" type="text/javascript"></script>
<script src="public/assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
<!-- END: PAGE SCRIPTS -->
<!-- END: LAYOUT/BASE/BOTTOM -->
<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '../../../../../www.google-analytics.com/analytics.js', 'ga');
	ga('create', 'UA-64667612-1', 'themehats.com');
	ga('send', 'pageview');
</script>
</body>


<!-- Mirrored from themehats.com/themes/jango/demos/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Aug 2019 15:29:25 GMT -->

</html>