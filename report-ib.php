﻿<?php include "navbar.php"; ?>
	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">Checkout</h3>
			<h4 class="">Page Sub Title Goes Here</h4>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li><a href="shop-checkout.html">Checkout</a></li>
			<li>/</li>
															<li class="c-state_active">Jango Components</li>
									
		</ul>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
		
<div class="c-content-box c-size-md c-bg-parallax" style="background-image: url(../../assets/base/img/content/backgrounds/bg-87.jpg)">
	<div class="container">
		<div class="c-content-feature-9">
			<!-- Begin: Title 1 component -->
			<div class="c-content-title-1">
				<h3 class="c-font-uppercase c-center c-font-bold">What we can do</h3>
				<div class="c-line-center c-theme-bg"></div>
				<p class="c-font-center">Lorem ipsum dolor consetuer elit</p>
			</div>
			<!-- End-->
			<ul class="c-list">
				<li class="wow animate fadeInUp">
					<div class="c-card">
						<i class="icon-rocket c-font-blue-1-5 c-font-22 c-bg-white c-float-left"></i>
						<div class="c-content c-content-left">
							<h3 class="c-theme-font c-font-uppercase c-font-bold">Full Sass Support</h3>
							<p>
								Lorem ipsum dolor consetuer adipicing sed diam ticidut erat votpat dolore
							</p>
						</div>
					</div>	
				</li>
				<li class="wow animate fadeInUp">
					<div class="c-card">
						<i class="icon-user c-font-blue-1-5 c-font-27 c-bg-white c-float-left"></i>
						<div class="c-content c-content-left">
							<h3 class="c-theme-font c-font-uppercase c-font-bold">Group Tasks</h3>					
							<p>
								Lorem ipsum dolor consetuer adipicing sed diam ticidut erat votpat dolore
							</p>
						</div>
					</div>	
				</li>
				<li class="wow animate fadeInUp">
					<div class="c-card">
						<i class="icon-basket c-font-blue-1-5 c-font-27 c-bg-white c-float-left"></i>
						<div class="c-content c-content-left">
							<h3 class="c-theme-font c-font-uppercase c-font-bold">Responsive UI</h3>					
							<p>
								Lorem ipsum dolor consetuer adipicing sed diam ticidut erat votpat dolore
							</p>
						</div>
					</div>	
				</li>
			</ul>
			<ul class="c-list">
				<li class="wow animate fadeInUp">
					<div class="c-card">
						<i class="icon-bulb c-font-blue-1-5 c-font-22 c-bg-white c-float-left"></i>
						<div class="c-content c-content-left">
							<h3 class="c-theme-font c-font-uppercase c-font-bold">Easy Management</h3>
							<p>
								Lorem ipsum dolor consetuer adipicing sed diam ticidut erat votpat dolore
							</p>
						</div>
					</div>	
				</li>
				<li class="wow animate fadeInUp">
					<div class="c-card">
						<i class="icon-pie-chart c-font-blue-1-5 c-font-27 c-bg-white c-float-left"></i>
						<div class="c-content c-content-left">
							<h3 class="c-theme-font c-font-uppercase c-font-bold">Quick Reports</h3>					
							<p>
								Lorem ipsum dolor consetuer adipicing sed diam ticidut erat votpat dolore
							</p>
						</div>
					</div>	
				</li>
				<li class="wow animate fadeInUp">
					<div class="c-card">
						<i class="icon-trophy c-font-blue-1-5 c-font-27 c-bg-white c-float-left"></i>
						<div class="c-content c-content-left">
							<h3 class="c-theme-font c-font-uppercase c-font-bold">Great Support</h3>					
							<p>
								Lorem ipsum dolor consetuer adipicing sed diam ticidut erat votpat dolore
							</p>
						</div>
					</div>	
				</li>
			</ul>
		</div>
	</div> 
</div>
<!-- END: CONTENT/FEATURES/FEATURES-9 -->


	<!-- END: PAGE CONTAINER -->
<?php include "footer.php"; ?>