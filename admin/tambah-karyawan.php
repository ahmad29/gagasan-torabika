<?php
include "layout/header-form.php";

if (isset($_POST['simpan'])) {
    $nik    = mysqli_real_escape_string($conn, $_POST['nik']);
    $nama_karyawan    = mysqli_real_escape_string($conn, $_POST['nama_karyawan']);
    $alamat    = mysqli_real_escape_string($conn, $_POST['alamat']);
    $grup    = mysqli_real_escape_string($conn, $_POST['grup']);
    $password    = mysqli_real_escape_string($conn, $_POST['password']);
    $cek = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM tb_karyawan WHERE nik='$nik'"));
    if ($cek > 0) {
        echo "<script>window.alert('NIK Sudah Terdaftar')
    window.history.go(-1)</script>";
    } else {
        $sql = "INSERT INTO tb_karyawan VALUES ('$nik','$password','$nama_karyawan','$alamat','$grup')";

        if (mysqli_query($conn, $sql)) {
            echo "<script>location.replace('data-karyawan.php?tambah=true')</script>";
        } else {
            echo "Error updating record: " . mysqli_error($conn);
        }
    }
}
?>


<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php
    include "sidebar.php";
    ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <!--section starts-->
            <h1>
                Form karyawan
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="#">Tambah karyawan</a>
                </li>

            </ol>
        </section>
        <!--section ends-->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-fw ti-move"></i> Form Tambah karyawan
                            </h3>
                            <span class="pull-right">
                                <i class="fa fa-fw ti-angle-up clickable"></i>
                                <i class="fa fa-fw ti-close removepanel clickable"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="#">
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">NIK</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" placeholder="NIK" name="nik" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">Nama Karyawan</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" placeholder="Nama Karyawan" name="nama_karyawan" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" placeholder="Password" name="password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" placeholder="Alamat" name="alamat" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">Grup</label>
                                    <div class="col-sm-10">
                                        <select name="grup" class="form-control" required>
                                            <option value=""> -- Pilih Grup -- </option>
                                            <option value="A"> A </option>
                                            <option value="B"> B </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">

                                    </label>
                                    <div class="col-sm-10 col-md-10">
                                        <button type="submit" name="simpan" class="btn btn-success"> Save</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!--main content ends-->
            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>


<?php
include "layout/footer-form.php";
?>