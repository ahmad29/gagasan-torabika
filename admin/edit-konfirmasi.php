<?php
include "layout/header-form.php";

if (isset($_POST['update'])) {
    $id_acc_gagasan    = mysqli_real_escape_string($conn, $_POST['id_acc_gagasan']);
    $status    = mysqli_real_escape_string($conn, $_POST['status']);
    if ($status == 'Disetujui') {
        $sql = "UPDATE acc_gagasan SET status = '$status', point='10', id_user='$_SESSION[id_user]' WHERE id_acc_gagasan = $id_acc_gagasan";
    } else {
        $sql = "UPDATE acc_gagasan SET status = '$status', id_user='$_SESSION[id_user]' WHERE id_acc_gagasan = $id_acc_gagasan";
    }
    if (mysqli_query($conn, $sql)) {
        echo "<script>location.replace('konfirmasi-gagasan.php?update=true')</script>";
    } else {
        echo "Error updating record: " . mysqli_error($conn);
    }
}

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $sql = "SELECT * FROM acc_gagasan WHERE id_acc_gagasan = $id";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        while ($data = mysqli_fetch_array($result)) {
            $id_acc_gagasan = $data['id_acc_gagasan'];
            $status = $data['status'];
        }
    }
}
?>


<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php
    include "sidebar.php";
    ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <!--section starts-->
            <h1>
                Form Konfirmasi Gagasan
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="#">Form Konfirmasi Gagasan</a>
                </li>

            </ol>
        </section>
        <!--section ends-->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-fw ti-move"></i> Form Konfirmasi Gagasan
                            </h3>
                            <span class="pull-right">
                                <i class="fa fa-fw ti-angle-up clickable"></i>
                                <i class="fa fa-fw ti-close removepanel clickable"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="#">
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" class="form-control" placeholder="alamat" name="id_acc_gagasan" value="<?php echo $id_acc_gagasan; ?>">

                                        <select class="form-control" name="status" required>

                                            <?php

                                            if ($status == 'Menunggu') {
                                                echo "<option value='Menunggu' selected> Menunggu</option>";
                                            } else {
                                                echo "<option value='Menunggu'> Menunggu</option>";
                                            }
                                            if ($status == 'Disetujui') {
                                                echo "<option value='Disetujui' selected> Disetujui</option>";
                                            } else {
                                                echo "<option value='Disetujui'> Disetujui</option>";
                                            }
                                            if ($status == 'Tidak Disetujui') {
                                                echo "<option value='Tidak Disetujui' selected> Tidak Disetujui</option>";
                                            } else {
                                                echo "<option value='Tidak Disetujui'> Tidak Disetujui</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">

                                    </label>
                                    <div class="col-sm-10 col-md-10">
                                        <button type="submit" name="update" class="btn btn-success"> Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!--main content ends-->
            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>


<?php

include "layout/footer-form.php";
?>