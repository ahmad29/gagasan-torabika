<?php session_start();
include '../config.php';
include '../fungsi/base_url.php';
if (isset($_POST['submit'])) {
    $errors     = array();
    $username = mysqli_real_escape_string($conn, $_POST['username']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);

    $sql    = "SELECT * FROM tb_user WHERE username = '$username' AND password = '$password' ";
    $result = mysqli_query($conn, $sql);
    $data   = mysqli_fetch_array($result);
    if (mysqli_num_rows($result) > 0) {
        if (empty($errors)) {
            $_SESSION['id_user']    = $data['id_user'];
            $_SESSION['username']      = $data['username'];
            $_SESSION['nama_lengkap']       = $data['nama_lengkap'];

            $_SESSION['bik']   = $data['bik'];


            echo "<script>location.replace('home.php')</script>";
        } else {
            echo "<script>alert('Gagal Login!');history.go(-1)</script>";
        }
    } else {
        echo "<script>alert('Username yang Anda masukkan tidak terdaftar!');history.go(-1)</script>";
    }
}

if (isset($_SESSION['username'])) {
    header("location:home.php");
}
?>

<!DOCTYPE html>
<html>


<!-- Mirrored from demo.vueadmintemplate.com/dark/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Apr 2019 07:15:32 GMT -->

<head>
    <title>::Admin Login::</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo $base_url ?>template/img/favicon.ico" />
    <!-- Bootstrap -->
    <link href="<?php echo $base_url ?>template/css/bootstrap.min.css" rel="stylesheet">
    <!-- end of bootstrap -->
    <!--page level css -->
    <link type="text/css" href="<?php echo $base_url ?>template/vendors/themify/css/themify-icons.css" rel="stylesheet" />
    <link href="<?php echo $base_url ?>template/vendors/iCheck/css/all.css" rel="stylesheet">
    <link href="<?php echo $base_url ?>template/vendors/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet" />
    <link href="<?php echo $base_url ?>template/css/login.css" rel="stylesheet">
    <!--end page level css-->
</head>

<body id="sign-in">
    <div class="preloader">
        <div class="loader_img"><img src="<?php echo $base_url ?>template/img/loader.gif" alt="loading..." height="64" width="64"></div>
    </div>
    <br><br><br>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 login-form">
                <div class="panel-header">
                    <h2 class="text-center">
                        <img src="../gambar/torabika1.png" width="100" alt="Logo">
                    </h2>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <form action="#" method="post">
                                <div class="form-group">
                                    <label for="email" class="sr-only"> Username</label>
                                    <input type="text" class="form-control  form-control-lg" name="username" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label for="password" class="sr-only">Password</label>
                                    <input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="Password">
                                </div>

                                <div class="form-group">
                                    <input type="submit" name="submit" value="Sign In" class="btn btn-primary btn-block" />
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- global js -->
    <script src="<?php echo $base_url ?>template/jquery.min.js"></script>
    <script src="<?php echo $base_url ?>template/bootstrap.min.js"></script>
    <!-- end of global js -->
    <!-- page level js -->
    <script type="text/javascript" src="<?php echo $base_url ?>template/vendors/iCheck/js/icheck.js"></script>
    <script src="<?php echo $base_url ?>template/vendors/bootstrapvalidator/js/bootstrapValidator.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo $base_url ?>template/js/custom_js/login.js"></script>
    <!-- end of page level js -->
</body>


<!-- Mirrored from demo.vueadmintemplate.com/dark/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Apr 2019 07:15:35 GMT -->

</html>