<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar-->
    <section class="sidebar">
        <div id="menu" role="navigation">
            <div class="nav_profile">
                <div class="media profile-left">
                    <a class="pull-left profile-thumb" href="#">
                        <img src="<?php echo $base_url ?>template/img/original.jpg" class="img-circle" alt="User Image"></a>
                    <div class="content-profile">
                        <h4 class="media-heading"><?php echo $_SESSION['username']; ?></h4>
                        <ul class="icon-list">
                            <li>
                                <a href="#">
                                    <i class="fa fa-fw ti-user"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-fw ti-lock"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-fw ti-settings"></i>
                                </a>
                            </li>
                            <li>
                                <a href="logout.php">
                                    <i class="fa fa-fw ti-shift-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="navigation">
                <li>
                    <a href="index.php">
                        <i class="menu-icon ti-home"></i>
                        <span class="mm-text ">Home</span>
                    </a>
                </li>
                <li>
                    <a href="data-karyawan.php">
                        <i class="menu-icon ti-user"></i>
                        <span class="mm-text ">Data Karyawan</span>
                    </a>
                </li>
                <li>
                    <a href="data-reward.php">
                        <i class="menu-icon ti-clipboard"></i>
                        <span class="mm-text ">Data Reward</span>
                    </a>
                </li>
                <li>
                    <a href="konfirmasi-gagasan.php">
                        <i class="menu-icon ti-clipboard"></i>
                        <span class="mm-text ">Konfirmasi Gagasan</span>
                        <?php
                        $sql = "SELECT COUNT(*) as jumlah FROM tb_gagasan a JOIN acc_gagasan b ON a.id_gagasan = b.id_gagasan WHERE b.status = 'Menunggu'
                ";
                        $result = mysqli_query($conn, $sql);

                        if (mysqli_num_rows($result) > 0) {
                            while ($data = mysqli_fetch_array($result)) {
                                ?>
                                <?php if ($data['jumlah'] != '0') { ?>
                                    <small class="badge"><?php echo $data['jumlah']; ?></small>
                                <?php } else { } ?>

                        <?php
                            }
                        }
                        ?>

                    </a>
                </li>
                <li class="menu-dropdown">
                    <a href="#">
                        <i class="menu-icon ti-briefcase"></i>
                        <span>
                            Data Gagasan
                        </span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="gagasan-disetujui.php">
                                <i class="fa fa-fw ti-brush"></i> Gagasan Disetujui
                            </a>
                        </li>
                        <li>
                            <a href="gagasan-tidak-disetujui.php">
                                <i class="fa fa-fw ti-layout-grid2"></i> Gagasan Tisak Disetujui
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-dropdown">
                    <a href="#">
                        <i class="menu-icon ti-briefcase"></i>
                        <span>
                            Laporan
                        </span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="lap-data-gagasan.php">
                                <i class="fa fa-fw ti-brush"></i> Data Gagasan
                            </a>
                        </li>
                        <li>
                            <a href="lap-klaim-reward.php">
                                <i class="fa fa-fw ti-layout-grid2"></i> Data Klaim Reward
                            </a>
                        </li>
                        <!-- <li>
                                <a href="#">
                                    <i class="fa fa-fw ti-tag"></i> Jumlah Pengunjung
                                </a>
                            </li> -->

                    </ul>
                </li>
                <li>
                    <a href="logout.php">
                        <i class="menu-icon ti-shift-right"></i>
                        <span class="mm-text ">Logout</span>
                    </a>
                </li>

            </ul>
            <!-- / .navigation -->
        </div>
        <!-- menu -->
    </section>
    <!-- /.sidebar -->
</aside>