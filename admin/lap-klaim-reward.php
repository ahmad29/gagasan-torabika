<?php
include "layout/header-form.php";
?>


<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php
    include "sidebar.php";
    ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <!--section starts-->
            <h1>
                Laporan Klaim Reward
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="#">Laporan Klaim Reward</a>
                </li>

            </ol>
        </section>
        <!--section ends-->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <br>
                        <div class="panel-body">

                            <form class="form-horizontal" role="form" method="POST" action="#">
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2">Dari Tanggal</label>
                                    <div class="col-sm-3">
                                        <input type="date" class="form-control" name="dari" required />
                                    </div>
                                    <label for="input-text" class="col-sm-2">Sampai Tanggal</label>
                                    <div class="col-sm-3">
                                        <input type="date" class="form-control" name="sampai" required />
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="submit" name="proses" class="btn btn-success"> Proses</button>
                                    </div>

                                </div>




                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            if (isset($_POST['proses'])) {
                $dari = $_POST['dari'];
                $sampai  = $_POST['sampai'];

                $query1        = "SELECT *
                            FROM riwayat_reward a JOIN tb_reward b ON a.id_reward = b.id_reward 
                            JOIN tb_karyawan c ON a.nik = c.nik
                            
                            WHERE  a.tanggal_riwayat BETWEEN '$dari' AND '$sampai'";
                $hasil1        = mysqli_query($conn, $query1);


                if (mysqli_num_rows($hasil1) == 0) {
                    echo "<center><h4>Tidak Ada Hasil</h4></center>";
                } else {

                    echo "
            
            <div class='box'>
        
          <div class='box-body table-responsive padding'>
            
            <div class='panel-heading' align='center'>
            <a href='print-klaim-reward.php?dari=$dari&&sampai=$sampai' target='_BLANK' class='btn btn-success'><i class='fa fa-print'></i> Cetak</a>
            </div>
            <table class='col-md-12 table-bordered table-striped table-condensed cf'>
      <thead class='cf'>
        <tr>
        <td align='center'>No.</td>
        <td align='center'>NIK</td>
        <td align='center'>Nama Karyawan</td>
        <td align='center'>Reward</td>
        <td align='center'>Tanggal</td>
          
          
        </tr>
      </thead>";

                    $no = 1;
                    $total = 0;
                    while ($data = mysqli_fetch_array($hasil1)) {



                        echo "
      <tbody>
        <tr>
          <td data-title='No.' align='center'>" . $no . "</td>
          </td><td data-title='Harga Diskon' align='center'>$data[nik]</td>
          </td><td data-title='Harga Diskon' align='center'>$data[nama]</td>
          </td><td data-title='Harga Diskon' align='center'>$data[nama_produk]</td>
          </td><td data-title='Harga Diskon' align='center'>$data[tanggal_riwayat]</td>
          
        </tr>";
                        $no++;
                    }
                    echo "
      
    
    </tbody>
          </table>";

                    ?>
            <?php
                }
            } ?>


            <!--main content ends-->
            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>


<?php
include "layout/footer-form.php";
?>