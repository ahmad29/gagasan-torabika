<?php
include "layout/header-form.php";

if (isset($_POST['update'])) {
    $id_reward    = mysqli_real_escape_string($conn, $_POST['id_reward']);
    $nama_produk    = mysqli_real_escape_string($conn, $_POST['nama_produk']);
    $jumlah    = mysqli_real_escape_string($conn, $_POST['jumlah']);
    $point    = mysqli_real_escape_string($conn, $_POST['point']);

    $sql = "UPDATE tb_reward SET nama_produk = '$nama_produk', jumlah = '$jumlah',
                point = '$point', id_user = '$_SESSION[id_user]' WHERE id_reward = $id_reward";

    if (mysqli_query($conn, $sql)) {
        echo "<script>location.replace('data-reward.php?update=true')</script>";
    } else {
        echo "Error updating record: " . mysqli_error($conn);
    }
}

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $sql = "SELECT * FROM tb_reward WHERE id_reward = $id";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        while ($data = mysqli_fetch_array($result)) {
            $id_reward = $data['id_reward'];
            $nama_produk = $data['nama_produk'];
            $jumlah = $data['jumlah'];
            $point = $data['point'];
        }
    }
}
?>


<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php
    include "sidebar.php";
    ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <!--section starts-->
            <h1>
                Form Reward
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="#">Edit Reward</a>
                </li>

            </ol>
        </section>
        <!--section ends-->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-fw ti-move"></i> Form Edit Reward
                            </h3>
                            <span class="pull-right">
                                <i class="fa fa-fw ti-angle-up clickable"></i>
                                <i class="fa fa-fw ti-close removepanel clickable"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="#">
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">Nama produk</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" class="form-control" placeholder="Nama produk" name="id_reward" value="<?php echo $id_reward; ?>">
                                        <input type="text" class="form-control" placeholder="Nama produk" name="nama_produk" value="<?php echo $nama_produk; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">jumlah</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" placeholder="jumlah" name="jumlah" value="<?php echo $jumlah; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">point</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" placeholder="point" name="point" value="<?php echo $point; ?>" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">

                                    </label>
                                    <div class="col-sm-10 col-md-10">
                                        <button type="submit" name="update" class="btn btn-success"> Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!--main content ends-->
            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>


<?php

include "layout/footer-form.php";
?>