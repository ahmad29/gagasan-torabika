<?php session_start();
include '../config.php';
include '../fungsi/base_url.php';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>PT. Torabika</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- <link rel="shortcut icon" href="<?php echo $base_url ?>template/img/favicon.ico" /> -->
    <link type="text/css" href="<?php echo $base_url ?>template/css/app.css" rel="stylesheet" />
    <link href="<?php echo $base_url ?>template/vendors/iCheck/css/all.css" rel="stylesheet" />
    <link href="<?php echo $base_url ?>template/vendors/bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>template/css/custom.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>template/css/custom_css/skins/skin-default.css" type="text/css" id="skin" />
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url ?>template/css/formelements.css">

</head>

<body class="skin-default">
    <div class="preloader">
        <div class="loader_img"><img src="<?php echo $base_url ?>template/img/loader.gif" alt="loading..." height="64" width="64"></div>
    </div>
    <header class="header">
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="index.html" class="logo">
                <img src="../gambar/torabika1.png" width="50" alt="logo" />
            </a>
            <div>
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <i class="fa fa-fw ti-menu"></i>
                </a>
            </div>

        </nav>
    </header>