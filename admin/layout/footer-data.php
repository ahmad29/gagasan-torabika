<script src="<?php echo $base_url ?>template/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/g/bootstrap@3.3.7,bootstrap.switch@3.3.2,jquery.nicescroll@3.6.0"></script>
<script src="<?php echo $base_url ?>template/js/app.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $base_url ?>template/vendors/sweetalert2/js/sweetalert2.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>template/js/custom_js/sweetalert.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>template/vendors/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>template/vendors/datatables/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo $base_url ?>template/js/custom_js/datatables_custom.js"></script>


</body>
</html>