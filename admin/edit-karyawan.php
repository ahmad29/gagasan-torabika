<?php
include "layout/header-form.php";

if (isset($_POST['update'])) {
    $nik    = mysqli_real_escape_string($conn, $_POST['nik']);
    $nama    = mysqli_real_escape_string($conn, $_POST['nama']);
    $password    = mysqli_real_escape_string($conn, $_POST['password']);
    $alamat    = mysqli_real_escape_string($conn, $_POST['alamat']);
    $grup    = mysqli_real_escape_string($conn, $_POST['grup']);

    $sql = "UPDATE tb_karyawan SET nama = '$nama', password = '$password',
                alamat = '$alamat', grup = '$grup' WHERE nik = $nik";

    if (mysqli_query($conn, $sql)) {
        echo "<script>location.replace('data-karyawan.php?update=true')</script>";
    } else {
        echo "Error updating record: " . mysqli_error($conn);
    }
}

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $sql = "SELECT * FROM tb_karyawan WHERE nik = $id";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        while ($data = mysqli_fetch_array($result)) {
            $nik = $data['nik'];
            $nama = $data['nama'];
            $password = $data['password'];
            $alamat = $data['alamat'];
            $grup = $data['grup'];
        }
    }
}
?>


<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php
    include "sidebar.php";
    ?>
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <!--section starts-->
            <h1>
                Form karyawan
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">
                        <i class="fa fa-fw ti-home"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="#">Edit karyawan</a>
                </li>

            </ol>
        </section>
        <!--section ends-->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-fw ti-move"></i> Form Edit karyawan
                            </h3>
                            <span class="pull-right">
                                <i class="fa fa-fw ti-angle-up clickable"></i>
                                <i class="fa fa-fw ti-close removepanel clickable"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="#">

                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">NIK</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" placeholder="NIK" name="nik" value="<?php echo $nik; ?>" readonly required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">Nama Karyawan</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" placeholder="Nama Karyawan" name="nama" value="<?php echo $nama; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" placeholder="Password" name="password" value="<?php echo $password; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" placeholder="Alamat" name="alamat" required><?php echo $alamat; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="input-text" class="col-sm-2 control-label">Grup</label>
                                    <div class="col-sm-10">
                                        <select name="grup" class="form-control" required>
                                            <option value=""> -- Pilih Grup -- </option>
                                            <option value="A" <?php if ($grup == 'A') {
                                                                    echo "selected";
                                                                } else { } ?>> A </option>
                                            <option value="B" <?php if ($grup == 'B') {
                                                                    echo "selected";
                                                                } else { } ?>> B </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">

                                    </label>
                                    <div class="col-sm-10 col-md-10">
                                        <button type="submit" name="update" class="btn btn-success"> Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!--main content ends-->
            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>


<?php

include "layout/footer-form.php";
?>