﻿<?php
include "navbar.php";
$query = "SELECT max(id_gagasan) as maxKode FROM tb_gagasan";
$hasil = mysqli_query($conn, $query);
$data = mysqli_fetch_array($hasil);
$idgagasan = $data['maxKode'];

$noUrut = (int) substr($idgagasan, 3, 3);
$noUrut++;
$char = "GG";
$idgagasan = $char . sprintf("%03s", $noUrut);


if (isset($_POST['gagasanstore'])) {
	$nik = $_SESSION['nik'];
	$gagasan = mysqli_real_escape_string($conn, $_POST['gagasan']);

	$sql = "INSERT INTO tb_gagasan VALUES ('$idgagasan','$gagasan','$nik',now())";

	if (mysqli_query($conn, $sql)) {

		$sql1 = "INSERT INTO acc_gagasan VALUES ('','$idgagasan','Menunggu','','','')";
		if (mysqli_query($conn, $sql1)) {
			echo "<script language='javascript'>alert('Input Gagasan Berhasil '); location.replace('list-ib.php')</script>";
		} else {
			echo "Error updating record: " . mysqli_error($conn);
		}
	} else {
		echo "Error updating record: " . mysqli_error($conn);
	}
}
?>
<!-- BEGIN: PAGE CONTAINER -->
<div class="c-layout-page">
	<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
	<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
		<div class="container">
			<div class="c-page-title c-pull-left">
				<h3 class="c-font-uppercase c-font-sbold">Input Gagasan</h3>

			</div>
			<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
				<li><a href="shop-checkout.html">Home</a></li>
				<li>/</li>
				<li class="c-state_active">Input Gagasan</li>

			</ul>
		</div>
	</div>
	<div class="c-content-box c-size-lg">
		<div class="container">
			<form class="#" method="post">
				<div class="row">

					<div class="col-md-12">
						<div class="c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
							<h1 class="c-font-bold c-font-uppercase c-font-24">Input Gagasan</h1>
							<ul class="c-order list-unstyled">
								<li class="row c-margin-b-15">
									<div class="col-md-2 c-font-20">
										<h2>Gagasan</h2>
									</div>
									<div class="col-md-10 c-font-20">
										<textarea class="form-control c-square c-theme" name="gagasan" rows="10" placeholder="Gagasan" required></textarea>
									</div>
								</li>

								<li class="row">
									<div class="col-md-2 c-font-20">

									</div>
									<div class="form-group col-md-10" role="group">
										<button type="submit" name="gagasanstore" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
										<!-- <button type="submit" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</button> -->
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- END: ORDER FORM -->
				</div>
			</form>
		</div>
	</div>
	<!-- END: PAGE CONTENT -->
</div>
<!-- END: PAGE CONTAINER -->
<?php include "footer.php"; ?>