﻿<?php include "navbar.php"; ?>
<!-- BEGIN: PAGE CONTAINER -->
<div class="c-layout-page">
	<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
	<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
		<div class="container">
			<div class="c-page-title c-pull-left">
				<h3 class="c-font-uppercase c-font-sbold">Riwayat</h3>
				<h4 class="">Gagasan</h4>
			</div>
			<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
				<li><a href="shop-checkout.html">Home</a></li>
				<li>/</li>
				<li class="c-state_active">Riwayat</li>

			</ul>
		</div>
	</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->

	<!-- BEGIN: PAGE CONTENT -->
	<div class="c-content-box c-size-md c-bg-white">
		<div class="container">


			<div class="c-content-panel">
				<div class="c-label">Riwayat</div>
				<div class="c-body">
					<div class="row">
						<div class="col-md-12">
							<table class="table">
								<caption>Gagasan.</caption>
								<thead>
									<tr>
										<th>No</th>
										<th>Isi Gagasan</th>
										<th>Status</th>

									</tr>
								</thead>
								<tbody>
									<?php
									$sql = "SELECT * FROM tb_gagasan a JOIN acc_gagasan b ON a.id_gagasan = b.id_gagasan ORDER BY a.tanggal ASC";
									$result = mysqli_query($conn, $sql);
									$no = 1;
									if (mysqli_num_rows($result) > 0) {
										while ($data = mysqli_fetch_array($result)) { ?>
											<tr>
												<th><?php echo $no; ?></th>
												<td><?php echo $data['isi_gagasan']; ?></td>
												<td><?php echo $data['status']; ?></td>

											</tr>
									<?php $no++;
										}
									} else {
										echo "Data Kosong";
									} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div><!-- BEGIN: PAGE CONTENT -->

		</div>
		<!-- END: PAGE CONTENT -->
	</div>
	<!-- END: PAGE CONTAINER -->
	<?php include "footer.php"; ?>