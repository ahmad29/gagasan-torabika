﻿<?php include "navbar.php"; ?>
<div class="c-layout-page">
	<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
	<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
		<div class="container">
			<div class="c-page-title c-pull-left">
				<h3 class="c-font-uppercase c-font-sbold">POINT</h3>
				<h4 class="">Cek Your Point Reward in Here</h4>
			</div>
			<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
				<li><a href="shop-product-wishlist.html">Home</a></li>
				<li>/</li>
				<li class="c-state_active">Point</li>

			</ul>
		</div>
	</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
	<div class="container">
		<div class="c-layout-sidebar-menu c-theme ">


			<ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
				<li class="c-dropdown c-open">
					<a href="javascript:;" class="c-toggler">Total Point</a>
					<ul class="c-dropdown-menu">
						<li class=""><br>
							<a href="#">
								<?php if (!empty($_SESSION['nik'])) {
									$sql = "SELECT total_point as jumlah FROM reedem_point WHERE nik = $_SESSION[nik] ";
									$result = mysqli_query($conn, $sql);

									if (mysqli_num_rows($result) > 0) {
										while ($data = mysqli_fetch_array($result)) {
											?>

											<h1 class=" c-font-bold"><?php echo $data['jumlah']; ?> Points</h1>



								<?php
										}
									}
								} else {
									echo '<h1 class="c-font-uppercase c-font-bold">0</h1>';
								}
								?>


							</a>
						</li><br><br>
						<li class="">
							<div class="form-group" role="group">&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="tukar-point.php" class="btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Tukar Point</a>
							</div>
						</li>
						<br><br>
					</ul>
				</li>
			</ul><!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
		</div>
		<div class="c-layout-sidebar-content ">
			<!-- BEGIN: PAGE CONTENT -->
			<div class="c-content-title-1">
				<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Reward</h3>
				<div class="c-line-left"></div>
			</div>
			<div class="c-shop-wishlist-1">
				<div class="c-border-bottom hidden-sm hidden-xs">
					<div class="row">
						<div class="col-md-3">
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">#</h3>
						</div>
						<div class="col-md-5">
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Gagasan</h3>
						</div>
						<div class="col-md-2">
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">Point</h3>
						</div>
						<div class="col-md-2">
							<h3 class="c-font-uppercase c-font-16 c-font-grey-2 c-font-bold">#</h3>
						</div>
					</div>
				</div>
				<?php if (!empty($_SESSION['nik'])) {
					$sql = "SELECT * FROM acc_gagasan a JOIN tb_gagasan b 
                        ON a.id_gagasan = b.id_gagasan WHERE a.point != 0 AND a.klaim_status = 0 
                        AND b.nik = $_SESSION[nik] ";
					$result = mysqli_query($conn, $sql);

					if (mysqli_num_rows($result) > 0) {
						while ($data = mysqli_fetch_array($result)) {
							?>





							<div class="c-border-bottom c-row-item">
								<div class="row">
									<div class="col-md-3 col-sm-12">
										<div class="c-content-overlay">

											<div class="c-bg-img-top-center c-overlay-object" data-height="height">
												<img width="70%" class="img-responsive" src="gambar/reward.gif">
											</div>
										</div>
									</div>
									<div class="col-md-5 col-sm-8">
										<ul class="c-list list-unstyled">
											<li class="c-margin-b-10"><?php echo $data['isi_gagasan']; ?></li>

										</ul>
									</div>
									<div class="col-md-2 col-sm-2">
										<p class="c-font-sbold c-font-18"><?php echo $data['point']; ?></p>
									</div>
									<div class="col-md-2 col-sm-2">
										<div class="form-group" role="group">
											<a href="klaim-reward.php?id_gagasan=<?php echo $data['id_gagasan']; ?>&&nik=<?php echo $data['nik']; ?>&&point=<?php echo $data['point']; ?>" class="btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Klaim&nbsp;Reward&nbsp;&nbsp;&nbsp;</a>
										</div>
									</div>
								</div>
							</div>

						<?php
								}
							} else { ?>
						<div class="c-border-bottom c-row-item">
							<div class="row">

								<div class="col-md-12 col-sm-12">
									<ul class="c-list list-unstyled">
										<li class="c-margin-b-10">Tidak Ada Reward</li>

									</ul>
								</div>

							</div>
						</div>

				<?php }
				}
				?>



			</div>
		</div>
	</div>
</div>
<!-- END: PAGE CONTAINER -->
<?php include "footer.php"; ?>