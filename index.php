﻿<?php include "navbar.php"; ?>

<div class="modal fade c-content-login-form" id="login-form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content c-square">
            <div class="modal-header c-no-border">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h3 class="c-font-24 c-font-sbold">Good Afternoon!</h3>
                <p>Let's make today a great day!</p>
                <form>
                    <div class="form-group">
                        <label for="login-email" class="hide">Email</label>
                        <input type="email" class="form-control input-lg c-square" id="login-email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="login-password" class="hide">Password</label>
                        <input type="password" class="form-control input-lg c-square" id="login-password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <div class="c-checkbox">
                            <input type="checkbox" id="login-rememberme" class="c-check">
                            <label for="login-rememberme" class="c-font-thin c-font-17">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>
                        <a href="javascript:;" data-toggle="modal" data-target="#forget-password-form" data-dismiss="modal" class="c-btn-forgot">Forgot Your Password ?</a>
                    </div>
                    <div class="clearfix">
                        <div class="c-content-divider c-divider-sm c-icon-bg c-bg-grey c-margin-b-20">
                            <span>or signup with</span>
                        </div>
                        <ul class="c-content-list-adjusted">
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-twitter">
                                  <i class="fa fa-twitter"></i>
                                  Twitter
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-facebook">
                                  <i class="fa fa-facebook"></i>
                                  Facebook
                                </a>
                            </li>
                            <li>
                                <a class="btn btn-block c-btn-square btn-social btn-google">
                                  <i class="fa fa-google"></i>
                                  Google
                                </a>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer c-no-border">                
                <span class="c-text-account">Don't Have An Account Yet ?</span>
                <a href="javascript:;" data-toggle="modal" data-target="#signup-form" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Signup!</a>
            </div>
        </div>
    </div>
</div><!-- END: CONTENT/USER/LOGIN-FORM -->

	<!-- BEGIN: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
<nav class="c-layout-quick-sidebar">
	<div class="c-header">
		<button type="button" class="c-link c-close">
		<i class="icon-login"></i>		
		</button>
	</div>
	<div class="c-content">
		<div class="c-section">
			<h3>JANGO DEMOS</h3>
			<div class="c-settings c-demos c-bs-grid-reset-space">	
				<div class="row">
					<div class="col-md-12">
						<a href="index.html" class="c-demo-container c-demo-img-lg">
							<div class="c-demo-thumb active">
								<img src="public/assets/base/img/content/quick-sidebar/default.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>	
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<a href="http://themehats.com/themes/jango/demos/corporate_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-left">
								<img src="public/assets/base/img/content/quick-sidebar/corporate_1.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>	
					</div>
					<div class="col-md-6">
						<a href="http://themehats.com/themes/jango/demos/agency_1/index.html" class="c-demo-container">
							<div class="c-demo-thumb  c-thumb-right">
								<img src="public/assets/base/img/content/quick-sidebar/corporate_1-onepage.jpg" class="c-demo-thumb-img"/>
							</div>
						</a>	
					</div>
				</div>			
			</div>
		</div>	
		<div class="c-section">
			<h3>Theme Colors</h3>
			<div class="c-settings">

				<span class="c-color c-default c-active" data-color="default"></span>
				
				<span class="c-color c-green1" data-color="green1"></span>
				<span class="c-color c-green2" data-color="green2"></span>
				<span class="c-color c-green3" data-color="green3"></span>

				<span class="c-color c-yellow1" data-color="yellow1"></span>
				<span class="c-color c-yellow2" data-color="yellow2"></span>
				<span class="c-color c-yellow3" data-color="yellow3"></span>
				
				<span class="c-color c-red1" data-color="red1"></span>
				<span class="c-color c-red2" data-color="red2"></span>
				<span class="c-color c-red3" data-color="red3"></span>

				<span class="c-color c-purple1" data-color="purple1"></span>
				<span class="c-color c-purple2" data-color="purple2"></span>
				<span class="c-color c-purple3" data-color="purple3"></span>

				<span class="c-color c-blue1" data-color="blue1"></span>
				<span class="c-color c-blue2" data-color="blue2"></span>
				<span class="c-color c-blue3" data-color="blue3"></span>

				<span class="c-color c-brown1" data-color="brown1"></span>
				<span class="c-color c-brown2" data-color="brown2"></span>
				<span class="c-color c-brown3" data-color="brown3"></span>

				<span class="c-color c-dark1" data-color="dark1"></span>
				<span class="c-color c-dark2" data-color="dark2"></span>
				<span class="c-color c-dark3" data-color="dark3"></span>
			</div>
		</div>	
		<div class="c-section">
			<h3>Header Type</h3>
			<div class="c-settings">				
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="boxed" value="boxed"/>
				<input type="button" class="c-setting_header-type btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="fluid" value="fluid"/>
			</div>
		</div>		
		<div class="c-section">
			<h3>Header Mode</h3>
			<div class="c-settings">			
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="fixed" value="fixed"/>
				<input type="button" class="c-setting_header-mode btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="static" value="static"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Mega Menu Style</h3>
			<div class="c-settings">			
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="dark" value="dark"/>
				<input type="button" class="c-setting_megamenu-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Font Style</h3>
			<div class="c-settings">			
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active" data-value="default" value="default"/>
				<input type="button" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase" data-value="light" value="light"/>
			</div>
		</div>
		<div class="c-section">
			<h3>Reading Style</h3>
			<div class="c-settings">	
				<a href="http://www.themehats.com/themes/jango/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase active">LTR</a>		
				<a href="http://www.themehats.com/themes/jango/rtl/" class="c-setting_font-style btn btn-sm c-btn-square c-btn-border-1x c-btn-white c-btn-sbold c-btn-uppercase ">RTL</a>		
			</div>
		</div>
	</div>
</nav><!-- END: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->

	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: LAYOUT/SLIDERS/REVO-SLIDER-4 -->
<section class="c-layout-revo-slider c-layout-revo-slider-4" dir="ltr">
    <div class="tp-banner-container c-theme">
        <div class="tp-banner rev_slider" data-version="5.0">
            <ul>
                 

                <!--BEGIN: SLIDE #2 -->
                <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                    <img 
                        alt="" 
                        src="public/assets/base/img/Coffee.jpg"
                        data-bgposition="center center" 
                        data-bgfit="cover" 
                        data-bgrepeat="no-repeat"
                        >   
                    
                </li>
                <!--END -->  

                <!--BEGIN: SLIDE #3 -->
                <li data-transition="fade" data-slotamount="1" data-masterspeed="700" data-delay="6000" data-thumb="">
                    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                    <img 
                        alt="" 
                        src="public/assets/base/img/kopi1.jpg"
                        data-bgposition="center center" 
                        data-bgfit="cover" 
                        data-bgrepeat="no-repeat"
                        class="visible-xs"
                        />  


                    
                </li>
                <!--END -->
            </ul>
        </div>
    </div>
</section><!-- END: LAYOUT/SLIDERS/REVO-SLIDER-4 -->

<!-- BEGIN: CONTENT/FEATURES/FEATURES-1 -->
<div class="c-content-box c-size-md c-bg-white">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="c-content-feature-1 wow animate fadeInUp">
					<div class="c-content-line-icon c-theme c-icon-screen-chart"></div>
					<h3 class="c-font-uppercase c-font-bold">Visi</h3>
					<p class="c-font-thin">- Melanjutkan Pertumabuhan Seiring Dengan Peningkatan Keuntungan</p>
					<p class="c-font-thin">- Menawarkan Produk - Produ Berkualitas</p>
					<p class="c-font-thin">- menjadi Pilihan Utama Untuk Pelanggan</p>
					<p class="c-font-thin">- Investasi Menjanjikan Bagi para Pekerja dan Pemilik</p>
					<p class="c-font-thin">- Menjadi Pilihan Utama Bagi Rekan Kerja Kami</p>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="c-content-feature-1 wow animate fadeInUp" data-wow-delay="0.2s">
					<div class="c-content-line-icon c-theme c-icon-support"></div>
					<h3 class="c-font-uppercase c-font-bold">Misi</h3>
					<p class="c-font-thin">Misi perusahaan berdasarkan prinsip dasar dari pertumbuhan dan tingkat keuntungan adalah untuk menigkatkan kemampuan pelanggan kami
                        agar dapat berkompetisi di pasar tingkat dunia dengan menyediakan alternatif ekonomis untuk semua kebutuhan mereka. <br> 
                        Pada intinya, kami akan berkembang menjadi pemimpin dalam industri makanan di indonesia. <br>
                        
                    </p>
				</div>
			</div>
			
		</div>
	</div> 
</div>
<!-- END: CONTENT/FEATURES/FEATURES-1 -->


<!-- BEGIN: CONTENT/FEATURES/FEATURES-11 -->
<div class="c-content-box c-size-md c-bg-parallax" style="background-image: url(public/assets/base/img/kopi1.jpg)">
	<div class="container">
		<div class="c-content-feature-8 c-opt-1">
			<ul class="c-grid">
				<li>
					<div class="c-card c-font-right c-bg-opacity-1">
						<h3 class="c-font-40 c-font-white c-font-bold c-font-uppercase">Energen</h3>
						<p class="c-font-white c-font-18">
                        pelopor minuman cereal
						</p>
					</div>
				</li>
				<li>
				<div class="c-card c-bg-opacity-1">
						<h3 class="c-font-40 c-font-white c-font-bold c-font-uppercase">Kopi</h3>
						<p class="c-font-white c-font-18">
                            Kopi Torabika Duo dan Duo Susu, pelopor coffee mix
						</p>
					</div>
				</li>
			</ul>
			<ul class="c-grid">
				<li>
					<div class="c-card c-font-right c-bg-opacity-1">
						<h3 class="c-font-40 c-font-white c-font-bold c-font-uppercase">Kopiko</h3>
						<p class="c-font-white c-font-18">
                        Permen Kopiko, pelopor permen kopi
						</p>
					</div>
				</li>
				<li>
					<div class="c-card c-img c-bg-img-center" style="background-image: url(public/assets/base/img/logo1.png); width="50%">
						<h3 class="c-font-white c-font-50 c-font-center">&nbsp;</h3>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div><!-- END: CONTENT/FEATURES/FEATURES-11 -->
<?php include "footer.php"; ?>