﻿<?php include "navbar.php"; ?>
<!-- BEGIN: PAGE CONTAINER -->
<div class="c-layout-page">
	<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
	<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
		<div class="container">
			<div class="c-page-title c-pull-left">
				<h3 class="c-font-uppercase c-font-sbold">Riwayat Penukaran</h3>
				<h4 class=""></h4>
			</div>
			<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
				<li><a href="shop-checkout.html">Home</a></li>
				<li>/</li>
				<li class="c-state_active">Riwayat Penukaran</li>

			</ul>
		</div>
	</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->

	<!-- BEGIN: PAGE CONTENT -->
	<div class="c-content-box c-size-md c-bg-white">
		<div class="container">


			<div class="c-content-panel">
				<div class="c-label">Riwayat Penukaran </div>
				<div class="c-body">
					<div class="row">
						<div class="col-md-12">
							<table class="table">
								<caption>List Product Yang Telah Anda Tukar.</caption>
								<thead>
									<tr>
										<th>No</th>
										<th>Product</th>
										<th>Total Point Yang Ditukar</th>
										<th>#</th>

									</tr>
								</thead>
								<tbody>
									<?php
									$sql = "SELECT * FROM riwayat_reward a JOIN tb_reward b ON a.id_reward = b.id_reward WHERE a.nik = $_SESSION[nik] ";
									$result = mysqli_query($conn, $sql);
									$no = 1;
									if (mysqli_num_rows($result) > 0) {
										while ($data = mysqli_fetch_array($result)) { ?>
											<tr>
												<th><?php echo $no; ?></th>
												<td><?php echo $data['nama_produk']; ?></td>
												<td><?php echo $data['point']; ?></td>

												<td>
													<div class="form-group" role="group">
														<a href="cetak-struk.php?id_riwayat=<?php echo $data['id_riwayat']; ?>" class="btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Cetak Struk</a>
													</div>
												</td>

											</tr>
									<?php $no++;
										}
									} else {
										echo "Data Kosong";
									} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div><!-- BEGIN: PAGE CONTENT -->

		</div>
		<!-- END: PAGE CONTENT -->
	</div>
	<!-- END: PAGE CONTAINER -->
	<?php include "footer.php"; ?>