-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03 Sep 2019 pada 16.23
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gagasan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `acc_gagasan`
--

CREATE TABLE `acc_gagasan` (
  `id_acc_gagasan` int(10) NOT NULL,
  `id_gagasan` varchar(10) NOT NULL,
  `status` varchar(20) NOT NULL,
  `point` int(4) NOT NULL,
  `id_user` int(10) NOT NULL,
  `klaim_status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `acc_gagasan`
--

INSERT INTO `acc_gagasan` (`id_acc_gagasan`, `id_gagasan`, `status`, `point`, `id_user`, `klaim_status`) VALUES
(3, 'GG001', 'Disetujui', 10, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `reedem_point`
--

CREATE TABLE `reedem_point` (
  `nik` int(10) NOT NULL,
  `total_point` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `reedem_point`
--

INSERT INTO `reedem_point` (`nik`, `total_point`) VALUES
(123456, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `riwayat_reward`
--

CREATE TABLE `riwayat_reward` (
  `id_riwayat` int(10) NOT NULL,
  `nik` int(10) NOT NULL,
  `id_reward` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `riwayat_reward`
--

INSERT INTO `riwayat_reward` (`id_riwayat`, `nik`, `id_reward`) VALUES
(2, 123456, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_gagasan`
--

CREATE TABLE `tb_gagasan` (
  `id_gagasan` varchar(10) NOT NULL,
  `isi_gagasan` text NOT NULL,
  `nik` int(10) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_gagasan`
--

INSERT INTO `tb_gagasan` (`id_gagasan`, `isi_gagasan`, `nik`, `tanggal`) VALUES
('GG001', 'Gagasan Adalah Gagasan', 123456, '2019-09-01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_karyawan`
--

CREATE TABLE `tb_karyawan` (
  `nik` int(10) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `grup` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_karyawan`
--

INSERT INTO `tb_karyawan` (`nik`, `password`, `nama`, `alamat`, `grup`) VALUES
(56789, 'sopyan', 'sopyan', 'karawaci', 'B'),
(123456, '123456', 'Ahmad', 'Cikupa', 'A'),
(290495, 'ahmad', 'ahmad', '', 'B');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_reward`
--

CREATE TABLE `tb_reward` (
  `id_reward` int(10) NOT NULL,
  `nama_produk` varchar(30) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `point` int(10) NOT NULL,
  `id_user` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_reward`
--

INSERT INTO `tb_reward` (`id_reward`, `nama_produk`, `jumlah`, `point`, `id_user`) VALUES
(2, 'Wafer Mayora', 50, 10, 1),
(3, 'Wafer Mayora Exclusif', 30, 10, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `nik` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `nama_lengkap`, `nik`) VALUES
(1, 'admin', 'admin', 'administrator', 123456);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acc_gagasan`
--
ALTER TABLE `acc_gagasan`
  ADD PRIMARY KEY (`id_acc_gagasan`);

--
-- Indexes for table `reedem_point`
--
ALTER TABLE `reedem_point`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `riwayat_reward`
--
ALTER TABLE `riwayat_reward`
  ADD PRIMARY KEY (`id_riwayat`);

--
-- Indexes for table `tb_gagasan`
--
ALTER TABLE `tb_gagasan`
  ADD PRIMARY KEY (`id_gagasan`);

--
-- Indexes for table `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `tb_reward`
--
ALTER TABLE `tb_reward`
  ADD PRIMARY KEY (`id_reward`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acc_gagasan`
--
ALTER TABLE `acc_gagasan`
  MODIFY `id_acc_gagasan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `riwayat_reward`
--
ALTER TABLE `riwayat_reward`
  MODIFY `id_riwayat` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_reward`
--
ALTER TABLE `tb_reward`
  MODIFY `id_reward` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
