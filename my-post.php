﻿<?php                   // Memulai session
include 'navbar.php';                     // Panggil koneksi ke database
?>

<br><br><br><br><br><br><br>


<div class="content">
<section class="bkg-light medium boxed">
<div class="container">
<div class="row">
<div class="col-12">
<div class="boxed-container my-account">
<div class="account-nav">
<ul class="subnav justify-content-center align-self-center h-100">
<li class="my-auto active"><a href="my-post.php">My Post</a></li>
<li class="my-auto"><a href="my-account.php">My Account</a></li>

</ul>
</div>
<div class="account-nav-mobile">
<ul>
<li class="active"><a href="javascript:;" data-toggle="collapse" data-target="#menu-options" aria-expanded="false" class="collapsed">My Ads</a></li>
<li class=""><a href="javascript:;" data-toggle="collapse" data-target="#menu-options" aria-expanded="false" class="collapsed">Favorite Ads</a></li>

</ul>
<div id="menu-options" class="menu-options collapse" aria-expanded="false">
<ul>
<li class="active"><a href="my-account-my-listing.html">My Ads</a></li>
<li class=""><a href="my-account-favorites.html">Favorite Ads</a></li>
</ul>
</div>
</div>
<div class="account-listing">
<ul class="table-sheet list-my-listings">
<li>
<ul>
<li class="image"></li>
<li class="name"><a href="#">Name <i class="fas fa-angle-down"></i></a></li>
<li class="data"><a href="#">Data <i class="fas fa-angle-down"></i></a></li>
<li class="category"><a href="#">Category <i class="fas fa-angle-down"></i></a></li>
<li class="status"><a href="#">Status <i class="fas fa-angle-down"></i></a></li>
<li class="actions"><a href="#"></a></li>
</ul>
</li>
<li>


<?php
$id_petani = $_SESSION['id_petani'];
// Memanggil data dari tabel produk diurutkan dengan id_produk secara DESC dan dibatasi sesuai $start dan $per_halaman
$data     = mysqli_query($conn, "SELECT * FROM tb_hasil_pertanian a 
                    JOIN tb_kategori b ON a.id_kategori = b.id_kategori 
                    JOIN tb_petani c ON a.id_petani = c.id_petani 
                    JOIN tb_alamat d ON c.id_alamat = d.id_alamat WHERE a.id_petani = $id_petani ORDER BY a.id_hasil_pertanian DESC ");
$numrows  = mysqli_num_rows($data);
?>

<?php
// Jika data ketemu, maka akan ditampilkan dengan While
if($numrows > 0)
{
  while($row = mysqli_fetch_assoc($data))
  {
    
?>

        <!-- awal -->
        <li>
<ul>
<li class="image"><span class="image-holder">
   
    <?php if( $row['nama_foto'] == NULL){ ?>
        <img class="lazyloaded" src="gambar/no-image.png" data-src="#" alt=""> 
    <?php }else{ ?>
        <img class="lazyloaded" src="gambar/<?php echo $row['nama_foto']; ?>" data-src="#" alt="">
    <?php } ?>
</span></li>
<li class="name">
<span class="name-holder"><a href="view-listing.html"><?php echo $row['nama_hasil_perkebunan']; ?></a></span>
<div class="d-lg-none">
<span class="data-holder"><i class="far fa-clock"></i> <?php echo $row['tanggal_post']; ?></span>
<span class="category-holder"><a href="category-list.html"><?php echo $row['nama_kategori']; ?></a></span>
<span class="status-holder active">
    <?php if( $row['status_aktif'] == 0 OR $row['status_aktif'] == 1){ ?>
        <i class="fas fa-calendar-check"></i> Active
    <?php }else{ ?>
        <i class="fas fa-calendar-minus"></i> Expired
    <?php } ?>
    
   

</span>
</div>
</li>
<li class="data"><span class="data-holder"><i class="far fa-clock"></i> <?php echo $row['tanggal_post']; ?></span></li>
<li class="category"><span class="category-holder"><a href="category-list.html"><?php echo $row['nama_kategori']; ?></a></span></li>
<li class="status"><span class="status-holder active">
<?php if( $row['status_aktif'] == 0 OR $row['status_aktif'] == 1){ ?>
        <i class="fas fa-calendar-check"></i> Active
    <?php }else{ ?>
        <i class="fas fa-calendar-minus"></i> Expired
    <?php } ?>
</span></li>
<li class="actions">
<ul>

<li><a href="#stats-listing-1" class="btn btn-secondary" data-toggle="collapse" aria-expanded="false"><i class="fas fa-chart-line"></i> Detail</a></li>
</ul>
</li>
<li class="stats">
<div id="stats-listing-1" class="stats-block collapse" aria-expanded="false">
<ul>
<li>Deskripsi: <?php echo $row['deskripsi']; ?></li>
<li>Luas Lahan: <?php echo $row['luas_lahan']; ?></li>
<li>Hasil Per Lahan: <?php echo $row['hasil_per_lahan']; ?></li>
<li>Panen: <?php echo $row['panen_permusim']; ?> Kali / Tahun</li>
</ul>
</div>
</li>
</ul>
</li>
        
        <!-- akhir -->
  
        <?php
  // Mengakhiri pengulangan while
  }
}

?>


</div>
</div>
</div>
</div>
</div>
</section>
</div>

 <?php                   // Memulai session
include 'footer.php';                     // Panggil koneksi ke database
?>

