﻿<?php include "navbar.php"; ?>
<!-- BEGIN: PAGE CONTAINER -->
<div class="c-layout-page">
	<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
	<div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
		<div class="container">
			<div class="c-page-title c-pull-left">
				<h3 class="c-font-uppercase c-font-sbold">Tukar Point</h3>
				<?php if (!empty($_SESSION['nik'])) {
					$sql = "SELECT total_point as jumlah FROM reedem_point WHERE nik = $_SESSION[nik] ";
					$result = mysqli_query($conn, $sql);

					if (mysqli_num_rows($result) > 0) {
						while ($data = mysqli_fetch_array($result)) {
							?>
							<h4 class="">Total point anda saat ini adalah <?php echo $data['jumlah']; ?> Points</h4>




				<?php
						}
					}
				} else {
					echo '<h4 class="">0</h4>';
				}
				?>

			</div>
			<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
				<li><a href="shop-checkout.html">Home</a></li>
				<li>/</li>
				<li class="c-state_active">Tukar Point</li>

			</ul>
		</div>
	</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->

	<!-- BEGIN: PAGE CONTENT -->
	<div class="c-content-box c-size-md c-bg-white">
		<div class="container">


			<div class="c-content-panel">
				<div class="c-label">Tukar Point </div>
				<div class="c-body">
					<div class="row">
						<div class="col-md-12">
							<table class="table">
								<caption>List Product Yang Bisa Ditukar.</caption>
								<thead>
									<tr>
										<th>No</th>
										<th>Product</th>
										<th>Stok</th>
										<th>Jumlah Point</th>
										<th>#</th>

									</tr>
								</thead>
								<tbody>
									<?php
									$sql = "SELECT * FROM tb_reward ORDER BY nama_produk";
									$result = mysqli_query($conn, $sql);
									$no = 1;
									if (mysqli_num_rows($result) > 0) {
										while ($data = mysqli_fetch_array($result)) { ?>
											<tr>
												<th><?php echo $no; ?></th>
												<td><?php echo $data['nama_produk']; ?></td>
												<td><?php echo $data['jumlah']; ?></td>
												<td><?php echo $data['point']; ?></td>
												<td>
													<div class="form-group" role="group">
														<a href="tukar-point-action.php?id_reward=<?php echo $data['id_reward']; ?>&&nik=<?php echo $_SESSION['nik']; ?>&&point=<?php echo $data['point']; ?>&&stok=<?php echo $data['jumlah']; ?>" class="btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Tukar</a>
													</div>
												</td>

											</tr>
									<?php $no++;
										}
									} else {
										echo "Data Kosong";
									} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div><!-- BEGIN: PAGE CONTENT -->

		</div>
		<!-- END: PAGE CONTENT -->
	</div>
	<!-- END: PAGE CONTAINER -->
	<?php include "footer.php"; ?>