<?php session_start();
ob_start();
include 'config.php';                     // Panggil koneksi ke database

$id_riwayat      =     mysqli_real_escape_string($conn, $_GET['id_riwayat']);
// Membuat join query 3 tabel: transaksi, transaksi_detail dan produk
$hasil_invoice =     mysqli_query($conn, "SELECT *
FROM riwayat_reward a JOIN tb_reward b ON a.id_reward = b.id_reward 
JOIN tb_karyawan c ON a.nik = c.nik WHERE a.id_riwayat = '$id_riwayat'

");
if (mysqli_num_rows($hasil_invoice) == 0) {
    die("<script>alert('Tidak Bisa Cetak Struk');location.replace('index.php')</script>");
}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Bagian halaman HTML yang akan konvert -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Invoice #<?php echo $faktur; ?></title>
    <style type="text/css">
        .tabel2 {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tabel2 tr.odd td {
            background-color: #f9f9f9;
        }

        .tabel2 th,
        .tabel2 td {
            padding: 4px 5px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border: 1px solid #dddddd;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <td>
                <font style="font-size: 15px; text-align: left"><br /><b>PT. Torabika Eka Semesta</b></font><br />
                <font style="font-size: 15px; text-align: left"><br />Struk Penukaran Hadiah
                </font>
            </td>
        </tr>
    </table>

    <?php
    $query1        = "SELECT *
    FROM riwayat_reward a JOIN tb_reward b ON a.id_reward = b.id_reward 
    JOIN tb_karyawan c ON a.nik = c.nik WHERE a.id_riwayat = '$id_riwayat'
          
          ";
    $hasil1        = mysqli_query($conn, $query1);
    $data1         = mysqli_fetch_assoc($hasil1);


    ?>


    <p> <strong>NIK : <?php echo $data1['nik']; ?></strong>
        <br> <strong>Nama Karyawan : <?php echo $data1['nama']; ?></strong>
        <br> <strong>Nama Produk: <?php echo $data1['nama_produk']; ?></strong>
        <br> <strong>Jumlah : 1</strong>
        <br><br><br><br>
        <br> <strong>Note : Hadiah Dapat DiTukarkan DI Koperasi Karyawan</strong>
    </p>
    <hr />

</body>

</html><!-- Akhir halaman HTML yang akan di konvert -->

<?php
// ob_get_clean = salah 1 fungsi dalam PHP
$content = ob_get_clean();
// Memanggil class HTML2PDF dari direktori html2pdf pada project kita
include 'html2pdf/html2pdf.class.php';
try {
    // Mengatur invoice dalam format HTML2PDF
    // Keterangan: L = Landscape/ P = Portrait, A4 = ukuran kertas, en = bahasa, false = kode HTML2PDF, UTF-8 = metode pengkodean karakter
    $html2pdf = new HTML2PDF('P', 'A4', 'en', false, 'UTF-8', array(10, 5, 10, 0));
    // Mengatur invoice dalam posisi full page
    $html2pdf->pdf->SetDisplayMode('fullpage');
    // Menuliskan bagian content menjadi format HTML
    $html2pdf->writeHTML($content);
    // Mencetak nama file invoice
    $html2pdf->Output('invoice.pdf');
}
// Kodingan HTML2PDF
catch (HTML2PDF_exception $e) {
    echo $e;
    exit;
}
?>