    <?php                   // Memulai session
include 'navbar.php';                     // Panggil koneksi ke database

if(isset($_POST['simpan']))
{   
    $nama_petani    = mysqli_real_escape_string($conn, $_POST['nama_petani']);
    $email    = mysqli_real_escape_string($conn, $_POST['email']);
    $username    = mysqli_real_escape_string($conn, $_POST['username']);
    $password    = mysqli_real_escape_string($conn, $_POST['password']);
    $alamat    = mysqli_real_escape_string($conn, $_POST['alamat']);
    $no_hp    = mysqli_real_escape_string($conn, $_POST['no_hp']);
    $no_ktp    = mysqli_real_escape_string($conn, $_POST['no_ktp']);
    $status    = mysqli_real_escape_string($conn, $_POST['status']);

    $sql = "INSERT INTO tb_petani VALUES ('','$nama_petani','$email','$username','$password','$alamat',
            '$no_hp','$no_ktp','1',now())";
    
    if(mysqli_query($conn, $sql)){
        echo "<script language='javascript'>alert('Registrasi Berhasil, Klik Ok Untuk Login !'); location.replace('login.php')</script>";
    }else{
        echo "Error updating record: " . mysqli_error($conn);
    }
}
?>
<br><br><br><br><br><br>
<div class="content">
<section class="bkg-light medium">
<div class="container">
<div class="row">
<div class="col-12">
<div class="boxed-container">
<div class="authentication">
<div class="block">
<form class="" role="form" method="POST" action="#">
<div class="row">
<div class="col-12">
 <div class="section-header text-center">
<h3>Join</h3>
</div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" name="nama_petani" value="" placeholder="Nama Lengkap">
</div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-12 col-md-6">
<div class="form-group">
<input type="email" class="form-control" name="email" value="" placeholder="Email">
 </div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-12 col-md-6">
<div class="row">
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" name="username" value="" placeholder="Username">
</div>
</div>
<div class="col-12 col-md-6">
<div class="form-group">
<input type="password" class="form-control" name="password" value="" placeholder="Password">
</div>

</div>

</div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-12 col-md-6">
<div class="form-group">
<select class="form-control" name="alamat" required>
                                    <option value=''> Pilih Alamat</option>
                                <?php
                                    $data     = mysqli_query($conn, "SELECT * FROM tb_alamat ");
                                    $numrows  = mysqli_num_rows($data);
                                    if($numrows > 0){
                                        while($row = mysqli_fetch_assoc($data)){   
                                    ?>
                                         <option value="<?php echo $row['id_alamat']; ?>"><?php echo $row['alamat']; ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                
                                    
                                               
                                
                                </select>
</div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-12 col-md-6">
<div class="row">
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" name="no_hp" value="" placeholder="No Handphone">
</div>
</div>
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" name="no_ktp" value="" placeholder="No KTP">
</div>

</div>

</div>
</div>
</div>

<div class="row justify-content-center">
<div class="col-12 col-md-6 col-lg-4">
<div class="text-center">
<button type="submit" class="btn btn-primary full-width" name="simpan" >Join</button>
</form>
<p>Already have an account? <a href="login.php">Sign in.</a></p>
</div>
</div>
</div>
</div>
<div class="block">

</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>

<?php                   // Memulai session
include 'footer.php';                     // Panggil koneksi ke database
?>