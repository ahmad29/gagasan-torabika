<?php                   // Memulai session
include 'navbar.php';                     // Panggil koneksi ke database
?>
<?php 

if(isset($_POST['simpan']))
{
    
    $id_kategori    = mysqli_real_escape_string($conn, $_POST['id_kategori']);
    $nama_hasil_perkebunan    = mysqli_real_escape_string($conn, $_POST['nama_hasil_perkebunan']);
    $deskripsi    = mysqli_real_escape_string($conn, $_POST['deskripsi']);
    $luas_lahan    = mysqli_real_escape_string($conn, $_POST['luas_lahan']);
    $hasil_per_lahan    = mysqli_real_escape_string($conn, $_POST['hasil_per_lahan']);
    $panen_permusim    = mysqli_real_escape_string($conn, $_POST['panen_permusim']);
    $pendapatan_lahan    = mysqli_real_escape_string($conn, $_POST['pendapatan_lahan']);
    $waktu_musim    = mysqli_real_escape_string($conn, $_POST['waktu_musim']);
    $id_petani = $_SESSION['id_petani'];
    $username = $_SESSION['username'];

  $cekdata = "SELECT nama_hasil_perkebunan FROM tb_hasil_pertanian WHERE nama_hasil_perkebunan = '$nama_hasil_perkebunan' ";
  $ada     = mysqli_query($conn, $cekdata);
  if(mysqli_num_rows($ada) > 0)
  {
    echo "<script>alert('ERROR: Judul telah terdaftar, silahkan pakai Judul lain!');history.go(-1)</script>";
  }
    else
    {
      
      $file_name    = $_FILES['img']['name']; // File adalah name dari tombol input pada form
      
      $file_tmp     = $_FILES['img']['tmp_name'];
      $lokasi       = 'gambar/'.$file_name.'';
      
      $gambar = $file_name;

        move_uploaded_file($file_tmp, $lokasi);
        $sql = "INSERT INTO tb_hasil_pertanian VALUES ('','$id_kategori','$nama_hasil_perkebunan',
        '$deskripsi','$luas_lahan','$hasil_per_lahan','$panen_permusim','$pendapatan_lahan',
        '$waktu_musim','$id_petani','$gambar','0', now(), '$username')";
        if(mysqli_query($conn, $sql))
        {
          echo "<script>alert('Insert data berhasil! Klik ok untuk melanjutkan');location.replace('index.php')</script>";
        }
          else
          {
            echo "Error updating record: " . mysqli_error($conn);
          }
      
    }
}

?>


<br><br><br><br><br><br>

<div class="content">
<section class="bkg-light medium">
<div class="container">
<div class="row">
<div class="col-12">
<div class="boxed-container post">
<div class="post-listing">
<div class="block">
<form action="#" method="post" enctype="multipart/form-data">
<div class="row">
<div class="col-12">
<div class="section-header text-center">
<h3>Post hasil perkebunan</h3>

</div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-12 col-xl-11">
<div class="row">
<div class="col-12 col-md-6">
<div class="form-group">
<select name="id_kategori" required>
<option value="">Kategori</option>
<?php
                $data     = mysqli_query($conn, "SELECT * FROM tb_kategori ");
                $numrows  = mysqli_num_rows($data);
                if($numrows > 0)
                    {
                         while($row = mysqli_fetch_assoc($data))
                        {   
            ?>
                    <option value="<?php echo $row['id_kategori']; ?>"><?php echo $row['nama_kategori']; ?></option>
                    
            <?php
                        }
                    }
            ?>

</select>
</div>
</div>
<div class="col-12 col-md-6">
<div class="form-group">
<input type="text" class="form-control" name="nama_hasil_perkebunan" value="" placeholder="Nama Hasil Perkebunan">
</div>
</div>
</div>
<div class="row">
<div class="col-12">
<div class="form-group">
<textarea  class="form-control" placeholder="Deskripsi" name="deskripsi"></textarea>
</div>
</div>
</div>
<div class="row">
<div class="col-12 col-md-6">
<div class="form-group">
<select name="luas_lahan" required>
  <option value="">Luas Lahan</option>
   <?php
  for ($x = 1; $x <= 100; $x++) {
    ?>
     <option value="<?php echo $x; ?>"><?php echo $x; ?> Hektar</option>";
      <?php
  }
  ?> 


</select>
</div>
</div>

<div class="col-12 col-md-4">
<div class="form-group">
<input type="text" class="form-control" name="hasil_per_lahan" value="" placeholder="Hasil Per Lahan">
</div>
</div>

<div class="col-12 col-md-2">
<div class="form-group">
<select name="satuan" required>
  <option value=""> Satuan</option>
  <option value=""> KG</option>
  <option value=""> TON</option>


</select>
</div>
</div>
</div>
<div class="row">
<div class="col-12 col-md-6 col-lg-4 col-xl-6">
<div class="form-group">
<input type="text" class="form-control" name="panen_permusim" value="" placeholder="Berapa kali Panen Pertahun ?">
</div>
</div>
<div class="col-12 col-md-6 col-lg-4 col-xl-6">
<div class="form-group">
<input type="text" class="form-control" name="pendapatan_lahan" value="" placeholder="Pendapatan Lahan">
</div>
</div>

</div>
<div class="row">
<div class="col-12 ">
<div class="form-group">
<input type="text" class="form-control" name="waktu_musim" value="" placeholder="Waktu Musim Dari Bulan - Sampai Bulan">
</div>
</div>


</div>
</div>
</div>
</div>

<div class="block">
<div class="row">
<div class="col-12">
<div class="section-header text-center">
<h3>Foto Hasil Pertanian</h3>
</div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-12 col-xl-11">
<div class="row">
<div class="col-12">
<div class="form-group">
<input type="file" name="img" id="img" required/>
<!-- <input  name="img" type="file"> -->
</div>
</div>
</div>
<div class="row justify-content-center">
<div class="col-6 col-lg-2">
<button class="btn btn-primary full-width" type="submit" name="simpan">Submit</button>
</div>
<div class="col-6 col-lg-2">
<button class="btn btn-default full-width" type="button">Preview</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
</section>
</div>

<?php                   // Memulai session
include 'footer.php';                     // Panggil koneksi ke database
?>